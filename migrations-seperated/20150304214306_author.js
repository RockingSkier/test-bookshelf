'use strict';

exports.up = function(knex, Promise) {
  return knex.schema.createTable('authors', function(table) {
    console.log('Generating `authors`');
    table.increments('id').primary();
    table.timestamps();
    table.string('first_name');
    table.string('last_name');
  });
};

exports.down = function(knex, Promise) {
  console.log('Dropping `authors`');
  return knex.schema.dropTable('authors');
};
