'use strict';

exports.up = function(knex, Promise) {
  return knex.schema.table('books', function (table) {
    console.log('Adding `books.author_id` relationship');
    table.integer('author_id')
      .references('id')
      .inTable('authors');
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.table('books', function (table) {
    console.log('Removing `books.author_id` relationship');
    table.dropColumn('author_id');
  });
};
