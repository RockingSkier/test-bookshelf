'use strict';

exports.up = function(knex, Promise) {
  return knex.schema.createTable('books', function(table) {
    console.log('Generating `books`');
    table.increments('id').primary();
    table.timestamps();
    table.string('title');
  });
};

exports.down = function(knex, Promise) {
  console.log('Dropping `books`');
  return knex.schema.dropTable('books');
};
