'use strict';

exports.up = function(knex, Promise) {
  return knex.schema.createTable('publishers', function(table) {
    console.log('Generating `publishers`');
    table.increments('id').primary();
    table.timestamps();
    table.string('name');
  });
};

exports.down = function(knex, Promise) {
  console.log('Dropping `publishers`');
  return knex.schema.dropTable('publishers');
};
