'use strict';

exports.up = function(knex, Promise) {
  return knex.schema.table('books', function (table) {
    console.log('Adding `books.publisher_id` relationship');
    table.integer('publisher_id')
      .references('id')
      .inTable('publishers');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.table('books', function (table) {
    console.log('Removing `books.publisher_id` relationship');
    table.dropColumn('publisher_id');
  });
};
