var _ = require('lodash')
var buildEndpointOptions = require('./build-endpoint-options')
var describeModel = require('./describe-model')
var statusCodes = require('./status-codes')
var handlers = require('./handlers')
var Joi = require('joi')
var path = require('path')

module.exports = exports = function (options) {
  options = options || {}

  var server = options.server
  var Model = options.Model

  var modelDescription = describeModel(Model, options);
  var modelTags = ['api', modelDescription.slug];

  var idAttribute = Model.prototype.idAttribute

  var baseUrl = options.baseUrl || path.join('/api')
  var modelUrl = options.modelUrl || path.join('/', modelDescription.slug)
  var collectionUrl = path.join('/', baseUrl, modelUrl);
  var itemUrl = path.join(collectionUrl, '/', '{' + idAttribute + '}');

  var endpoints = buildEndpointOptions(options.endpoints);

  var paramValidation = {}
  paramValidation[idAttribute] = Joi.number()
    .required()
    .description(idAttribute + ' for the ' + modelDescription.name)

  var queryValidation = {
    'per-page': Joi.number()
      .integer()
      .min(1)
      .default(endpoints.list.perPage),
    'page': Joi.number()
      .integer()
      .min(1)
      .default(1)
  }

  if (endpoints.list) {
    server.route({
      method: ['GET'],
      path: collectionUrl,
      config: {
        description: 'Get a list of ' + modelDescription.pluralName,
        tags: modelTags,
        handler: handlers.list(Model, endpoints),
        plugins: {
          'hapi-swagger': {
            responseMessages: statusCodes.pick([200, 400, 500])
          }
        },
        validate: {
          query: _.pick(queryValidation, ['per-page', 'page'])
        }
      }
    })
  }

  if (endpoints.create) {
    server.route({
      method: ['POST'],
      path: collectionUrl,
      config: {
        description: 'Create ' + modelDescription.definitiveArticle + ' ' + modelDescription.name,
        tags: modelTags,
        handler: handlers.create(Model, endpoints),
        plugins: {
          'hapi-swagger': {
            responseMessages: statusCodes.pick([201, 500])
          }
        }
      }
    })
  }

  if (endpoints.detail) {
    server.route({
      method: ['GET'],
      path: itemUrl,
      config: {
        description: 'Get details of ' + modelDescription.definitiveArticle + ' ' + modelDescription.name,
        tags: modelTags,
        handler: handlers.detail(Model, endpoints),
        plugins: {
          'hapi-swagger': {
            responseMessages: statusCodes.pick([200, 400, 404, 500])
          }
        },
        validate: {
          params: _.pick(paramValidation, ['id'])
        }
      }
    })
  }

  if (endpoints.update) {
    server.route({
      method: ['PUT', 'PATCH'],
      path: itemUrl,
      config: {
        description: 'Update ' + modelDescription.definitiveArticle + ' ' + modelDescription.name,
        tags: modelTags,
        handler: handlers.update(Model, endpoints),
        plugins: {
          'hapi-swagger': {
            responseMessages: statusCodes.pick([200, 400, 500])
          }
        },
        validate: {
          params: _.pick(paramValidation, ['id'])
        }
      }
    })
  }

  if (endpoints.delete) {
    server.route({
      method: ['DELETE'],
      path: itemUrl,
      config: {
        description: 'Delete ' + modelDescription.definitiveArticle + ' ' + modelDescription.name,
        tags: modelTags,
        handler: handlers.delete(Model, endpoints),
        plugins: {
          'hapi-swagger': {
            responseMessages: statusCodes.pick([204, 400, 500])
          }
        },
        validate: {
          params: _.pick(paramValidation, ['id'])
        }
      }
    })
  }

}
