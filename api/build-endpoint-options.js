var _ = require('lodash')

module.exports = exports = function buildEndpointOptions (endpointOptions) {
  endpointOptions || (endpointOptions = {})

  return {
    list: _.defaults(endpointOptions.list || {}, {
      fetchOptions: {},
      page: 1,
      perPage: 10
    }),

    create: _.defaults(endpointOptions.create || {}, {}),

    detail: _.defaults(endpointOptions.detail || {}, {
      fetchOptions: {}
    }),

    delete: _.defaults(endpointOptions.delete || {}, {}),

    update: _.defaults(endpointOptions.update || {}, {})
  }
}
