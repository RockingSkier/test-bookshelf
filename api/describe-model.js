var _ = require('lodash')
var indefiniteArticle = require('indefinite-article')
var pluralize = require('pluralize')

module.exports = exports = function describeModel (Model, options) {
  var name = options.name || Model.prototype.tableName.replace(/s$/, '')
  var pluralName = options.pluralName || pluralize(name)

  return {
    name: name,
    pluralName: pluralName,
    definitiveArticle: indefiniteArticle(name),
    slug: _.kebabCase(_.deburr(pluralName))
  }
}
