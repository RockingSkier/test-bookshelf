var _ = require('lodash')

module.exports = exports = {
  errors: {
    200: { code: 200, message: 'OK'},
    201: { code: 201, message: 'Created'},
    204: { code: 204, message: 'No Content'},
    400: { code: 400, message: 'Bad Request'},
    404: { code: 404, message: 'Not Found'},
    500: { code: 500, message: 'Internal Server Error'}
  },

  pick: function (codes) {
    return _.chain(this.errors).pick(codes).values().value()
  }
}
