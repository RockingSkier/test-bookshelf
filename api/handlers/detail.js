var Boom = require('boom')

module.exports = exports = function buildDetailHandler (Model, endpointOptions) {
  return function detailHandler (request, reply) {
    Model
      .forge({
        id: request.params.id
      })
      .fetch(endpointOptions.detail.fetchOptions)
      .then(function (model) {
        if (!model) {
          return reply(Boom.notFound())
        }

        return reply(model.toJSON())
      })
      .catch(function (/*err*/) {
        return reply(Boom.badImplementation())
      })
  }
}
