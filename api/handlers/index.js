module.exports = exports = {
  list: require('./list'),
  create: require('./create'),
  detail: require('./detail'),
  update: require('./update'),
  delete: require('./delete')
}
