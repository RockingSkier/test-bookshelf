var Boom = require('boom')

module.exports = exports = function buildDeleteHandler (Model, endpointOptions) {
  return function (request, reply) {
    Model
      .forge({
        id: request.params.id
      })
      .destroy()
      .then(function () {
        reply()
          .code(204)
      })
      .catch(function (/*err*/) {
        return reply(Boom.badImplementation());
      })
  }
}
