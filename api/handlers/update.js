var Boom = require('boom')

module.exports = exports = function buildUpdateHandler (Model, endpointOptions) {
  return function updateHandler (request, reply) {
    var payload
    var saveOptions = {}

    if (request.method === 'patch') {
      saveOptions.patch = true
    }

    Model
      .forge({
        id: request.params.id
      })
      .fetch()
      .then(function (model) {
        if (!model) {
          return reply(Boom.notFound())
        }

        // Ensure `id` isn't overridden
        payload = _.omit(request.payload, ['id'])

        model
          .save(payload, saveOptions)
          .then(function (model) {
            return reply(model.toJSON())
          })
      })
      .catch(function (/*err*/) {
        return reply(Boom.badImplementation())
      })
  }
}
