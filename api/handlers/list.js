var Boom = require('boom')

module.exports = exports = function buildListHandler (Model, endpointOptions) {
  return function listHandler (request, reply) {
    var perPage = request.query['per-page']
    var page = request.query.page;

    Model
      .query({
        limit: perPage,
        offset: (page - 1) * perPage
      })
      .fetchAll(endpointOptions.list.fetchOptions)
      .then(function (models) {
        return reply(models.toJSON())
      })
      .catch(function (/*err*/) {
        return reply(Boom.badImplementation())
      })
  }
}
