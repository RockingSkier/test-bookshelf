var Boom = require('boom')

module.exports = exports = function buildCreateHandler (Model, endpointOptions) {
  return function createHandler (request, reply) {
    Model
      .forge(request.payload)
      .save()
      .then(function (model) {
        reply(model.toJSON())
          .code(201)
      })
      .catch(function (/*err*/) {
        return reply(Boom.badImplementation())
      })
  }
}
