module.exports = exports = function (bookshelf) {
  return {
    Author: require('./author')(bookshelf),
    Book: require('./book')(bookshelf),
    Publisher: require('./publisher')(bookshelf)
  }
}
