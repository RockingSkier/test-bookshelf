var baseModel = require('./base-model')

module.exports = exports = function (bookshelf) {
  bookshelf.plugin('registry')

  require('./author');
  require('./publisher');

  var BaseModel = baseModel(bookshelf)

  return bookshelf.model('Book') || bookshelf.model('Book', BaseModel.extend({
    tableName: 'books',

    defaults: {
      title: null
    },

    author: function() {
      return this.belongsTo('Author');
    },

    publisher: function() {
      return this.belongsTo('Publisher');
    }
  }))
}
