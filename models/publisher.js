var baseModel = require('./base-model')

module.exports = exports = function (bookshelf) {
  bookshelf.plugin('registry')

  var BaseModel = baseModel(bookshelf)

  return bookshelf.model('Publisher') || bookshelf.model('Publisher', BaseModel.extend({
    tableName: 'publishers',

    defaults: {
      name: null
    },

    books: function() {
      return this.hasMany('Book');
    }
  }))
}
