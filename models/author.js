var baseModel = require('./base-model')

module.exports = exports = function (bookshelf) {
  bookshelf.plugin('registry')

  var BaseModel = baseModel(bookshelf)

  return bookshelf.model('Author') || bookshelf.model('Author', BaseModel.extend({
    tableName: 'authors',

    defaults: {
      firstName: null,
      lastName: null
    },

    books: function() {
      return this.hasMany('Book');
    }
  }))
}
