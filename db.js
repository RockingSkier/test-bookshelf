var Bookshelf = require('bookshelf')
var Knex = require('knex')
var knexfile = require('./knexfile')

var knex = Knex(knexfile.development)
var bookshelf = Bookshelf(knex)

require('./models')(bookshelf)

module.exports = exports = bookshelf
