'use strict';

exports.up = function (knex, Promise) {
  return knex.schema.createTable('authors', function (table) {
      table.increments('id').primary();
      table.timestamps();
      table.string('first_name');
      table.string('last_name');
    })
    .createTable('publishers', function (table) {
      table.increments('id').primary();
      table.timestamps();
      table.string('name');
    })
    .createTable('books', function(table) {
      table.increments('id').primary();
      table.timestamps();
      table.string('title');

      // Relationships
      table.integer('author_id')
        .references('id')
        .inTable('authors');
      table.integer('publisher_id')
        .references('id')
        .inTable('publishers');
    });
};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('authors')
    .dropTable('books')
    .dropTable('publishers');
};
