var Blipp = require('blipp')
var Hapi = require('hapi')
var hapiSwagger = require('hapi-swagger')

var server = new Hapi.Server()
var db = require('./db')

server.connection({
  host: 'localhost',
  port: 8000
})

server.route({
  method: 'GET',
  path: '/',
  config: {
    description: 'Home page',
    handler: function (request, reply) {
      reply('Welcome')
    }
  }
})

require('./api')({
  server: server,
  Model: db.model('Author')
})

require('./api')({
  server: server,
  Model: db.model('Book'),
  endpoints: {
    list: {
      perPage: 5
    },
    detail: {
      fetchOptions: {
        withRelated: ['author', 'publisher']
      }
    },
    delete: false
  }
})

require('./api')({
  server: server,
  Model: db.model('Publisher')
})

server.register([
  Blipp,
  {
    register: hapiSwagger,
    options: {
      basePath: 'http://localhost:8000',
      apiVersion: '1'
    }
  }
], function (err) {
  if (err) {
    throw err
  }
  server.start(function () {
    console.log('Server running at:', server.info.uri)
  })
})
