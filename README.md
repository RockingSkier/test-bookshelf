# Bookshelf Test

A small test app for trying out [Bookshelf JS](http://bookshelfjs.org/) and [Knex](http://knexjs.org/)

By Ben

*Because learning.*


# Install

    git clone git@bitbucket.org:RockingSkier/bookshelf-test.git
    cd bookshelf-test

    npm install             # Install dependencies
    
    # npm install -g knex   # Install knex globally
    # Or use `./node_modules/knex/lib/bin/cli.js` instead of the `knex` command

    knex migrate:latest     # Migrate database
    knex seed:run           # Add seed data
    node index.js           # Start server with Node.js
    # iojs index.js         # Alternatively start server with io.js

All routes listed in console

* [http://localhost:8000/api/authors](http://localhost:8000/api/authors)
* [http://localhost:8000/api/books](http://localhost:8000/api/books)
* [http://localhost:8000/api/publishers](http://localhost:8000/api/publishers)
* Swagger documentation: [http://localhost:8000/documentation](http://localhost:8000/documentation)
