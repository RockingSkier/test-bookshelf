'use strict';

exports.seed = function (knex, Promise) {
  return Promise.join(
    // Deletes ALL existing entries
    knex('publishers').del(),

    // Inserts seed entries
    knex('publishers').insert({ id: 1, name: 'Book Makers Ltd' }),
    knex('publishers').insert({ id: 2, name: 'Publish Publish Publish' })
  );
};
