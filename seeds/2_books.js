'use strict';

exports.seed = function (knex, Promise) {
  return Promise.join(
    // Deletes ALL existing entries
    knex('books').del(),

    // Inserts seed entries
    knex('books').insert({ id: 1, title: 'Book' }),
    knex('books').insert({ id: 2, title: 'Magazine' }),
    knex('books').insert({ id: 3, title: 'Poetry' }),
    knex('books').insert({ id: 4, title: 'Novel' })
  );
};
