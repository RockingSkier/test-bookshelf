'use strict';

exports.seed = function(knex, Promise) {
    return Promise.join(
        knex('books')
          .where({ id: 1 })
          .update({ author_id: 1, publisher_id: 1 }),

        knex('books')
          .where({ id: 2 })
          .update({ author_id: 1, publisher_id: 2 }),

        knex('books')
          .where({ id: 3 })
          .update({ author_id: 2, publisher_id: 1 }),

        knex('books')
          .where({ id: 4 })
          .update({ author_id: 2, publisher_id: 2 })
    );
};
