'use strict';

exports.seed = function (knex, Promise) {
  return Promise.join(
    // Deletes ALL existing entries
    knex('authors').del(),

    // Inserts seed entries
    knex('authors').insert({ id: 1, first_name: 'Word', last_name: 'Smith' }),
    knex('authors').insert({ id: 2, first_name: 'Page', last_name: 'Turner' })
  );
};
